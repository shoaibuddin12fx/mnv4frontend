import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/database';

@Injectable({
  providedIn: 'root'
})
export class UserControlService {

  constructor() { }
  
  createNewUser(user: any) {
    user.createdAt = new Date().getTime();
	
	user.displayName = user.name;
	user.facebook = "assets/imgs/profile.jpg";
	user.lastName = user.lastname;
	user.address = "user address";
	user.type = "customer";
	
    delete user.pass; delete user.pass2;
	
	return this.getDatabaseUrl('users/' + user.uid).set(user);
    
  }

  getDatabaseUrl(url: string): firebase.database.Reference {
    return firebase.database().ref(url);
  }

  getUserInfo(uid: number) {
    
	return this.getDatabaseUrl('users/' + uid);
	
  }

  getInformationUserBy(by, param) {

    return this.getDatabaseUrl('users/').orderByChild(by).equalTo(param);
  }

  

  updateUserInfo(uid: number, data: any) {
    return this.getDatabaseUrl('users/' + uid).update(data);
  }
}
