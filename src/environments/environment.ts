// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  AdMobFreeBannerConfig: {
    isTesting: true,
    autoShow: true,
  },
  firebase: {
    // apiKey: "AIzaSyB7NxbCEmecSMh9sHpR-zimWZ0sL0ECmQk",
    // authDomain: "multivendorproject.firebaseapp.com",
    // databaseURL: "https://multivendorproject.firebaseio.com",
    // projectId: "multivendorproject",
    // storageBucket: "",
    // messagingSenderId: "967910052623",
    // appId: "1:967910052623:web:dfa9ded60a92dbaa"
    apiKey: 'AIzaSyDoKEwE9HuIrUvELx7Kz0HocXSbeo6G-Cc',
    authDomain: 'multivend-kreate.firebaseapp.com',
    databaseURL: 'https://multivend-kreate.firebaseio.com',
    projectId: 'multivend-kreate',
    storageBucket: 'multivend-kreate.appspot.com',
    messagingSenderId: '1031340289743',
    appId: '1:1031340289743:web:3588188762db30277291ba',
    measurementId: 'G-G6GE9D8XEX',
  },
  stripe_publish_key: 'pk_test_nqykHcHCdCnWPJCD6pguqShK',
  google_project_number: '762391382612',
  fb_app: 571610369618746,
  fb_v: 'v3.2',
  paypal_sandbox_client_id:
    'Ac-QK_Lkar46qQDWcp1kega6aPk13SxXv3dkCVX7A2Nlw7BViP3JyDUQQg-6W386yjgaeEHTuaO9BxGx',
  paypal_live_client_id: '',
  languages: {
    en: 'English',
    vi: 'Vietnamese',
  },
  menu: [
    {
      name: 'Multi Shops',
      path: '/home',
      component: 'HomePage',
      icon: 'tablet-portrait',
    },
    {
      name: 'MyCart',
      path: '/mycart',
      component: 'CartPage',
      icon: 'md-basket',
    },
    {
      name: 'Product Search',
      path: '/food-search',
      component: 'FoodSearchPage',
      icon: 'tv',
    },
    {
      name: 'MyOrder',
      path: '/orders',
      component: 'OrdersPage',
      icon: 'md-clipboard',
    },
    {
      name: 'Map',
      path: '/map',
      component: 'MapPage',
      icon: 'compass',
    },
    {
      name: 'WishList',
      path: '/wishlist',
      component: 'WishlistPage',
      icon: 'md-heart-empty',
    },
    {
      name: 'MyAddresses',
      path: '/my-address',
      component: 'MyAddressPage',
      icon: 'create',
    },
    {
      name: 'ChatList',
      path: '/chat-list',
      component: 'ChatListPage',
      icon: 'chatboxes',
    },
    {
      name: 'MyProfile',
      path: '/profile',
      component: 'ProfilePage',
      icon: 'construct',
    },
    {
      name: 'ForgotPassword',
      path: '/forgot',
      component: 'ForgotPage',
      icon: 'lock',
    },
    {
      name: 'Languages',
      path: '/translate',
      component: 'TranslatePage',
      icon: 'globe',
    },
  ],

  themes: [
    {
      name: 'Green',
      primary: '#a5c331',
      secondary: '#ff5a65',
      tertiary: '#45ba25',
      light: '#e3efd5',
      medium: '#b4c27c',
      dark: '#181a14',
    },
  ],
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
