import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupWithMobilePage } from './signup-with-mobile.page';

describe('SignupWithMobilePage', () => {
  let component: SignupWithMobilePage;
  let fixture: ComponentFixture<SignupWithMobilePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupWithMobilePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupWithMobilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
