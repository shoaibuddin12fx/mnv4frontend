import { Component , OnInit} from '@angular/core';
import { NavController, LoadingController, ToastController, Events, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service';

import { Values } from '../../providers/values';

import { CallNumber } from '@ionic-native/call-number/ngx';

import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import * as firebase from 'firebase';

import { TranslateService } from '@ngx-translate/core';

import * as _ from 'lodash';

declare var google;
declare var map;
declare var infoWindow;

@Component({
  selector: 'app-search-products',
  templateUrl: './search-products.page.html',
  styleUrls: ['./search-products.page.scss'],
})
export class SearchProductsPage implements OnInit {

  map : any;
	image: any;
	dataInfo: any;
	userList: any;
	distanceRestaurant: any;
  
	movieDetailPage = 'MovieDetailPage';
	informationDetailPage = 'InformationDetailPage';

	offset: number = 0;
  
	public layouticon:any;
	public layout:any;
	
	currentUser: any;
	myOrderList: any;
	id:any;
	
	params:any = {};

	
	searchedItem: any;
	public items: any;
	
	public chats:any = {};

	public nearestHotels:any = {};
	
	public nearestHotels2:any = {};
	
	public nearestHotels3:any = {};

	public featuredHotels:any = {};
	
	public fromPrice: any;
	
	public toPrice: any;

  constructor(public events: Events, 
		public toastCtrl: ToastController, 
		private storage: Storage, 
		public navCtrl: NavController,
		public service: ServiceProvider,
		public loadingCtrl: LoadingController,
		private callNumber: CallNumber,
		public values: Values,
		public router: Router,
		public geo:Geolocation,
		public platform: Platform,
		private translate: TranslateService) { 
		
				let that = this;
			  let map : any;
			  let infoWindow : any;
			  let beachMarker: any;
			  let image: any;
			  
			  let options = {
				  frequency: 3000,
				  enableHighAccuracy: true
				};
				
				
						
				this.fromPrice = this.values.fromPrice;
							
				this.toPrice = this.values.toPrice;
				
				console.log(this.fromPrice);
				console.log(this.toPrice);
	 
	 
				 this.geo.getCurrentPosition(options).then(resp =>{
						
						
						var uids = firebase.auth().currentUser.uid;

						firebase.database().ref('/users').child(uids).update({
								lat: resp.coords.latitude,
								lng: resp.coords.longitude,
							});
								
						
					

					}).catch(() =>{
						console.log("Error to get location");
					});
		
					that.platform.ready().then(() => {
					  
					  var options = {
					  timeout: 5000
				  };
					  
					
				  });
		
		
		}

  ionViewWillEnter(){
				   var markers = [];
		 var distance = [];
		
	  
	  
	  this.chats = [];
	  
	  
	  
	  
	  this.service.getAllChooseItems().on('value', snapshot =>{
    
				console.log(snapshot.val());
				console.log(snapshot.key);
				
				
				
				var key = snapshot.key;
				
				var val = snapshot.val();
				
				
				this.chats = [];
				
					snapshot.forEach( snap =>{
						
						
						
						
						
						
						
						
						
						
						
						
						distance.push(calcDistance(snap.val().restaurant_lat,snap.val().restaurant_long,snap.key,snap) + " kilometers away");

						
						var userLists: any;

						this.chats.push({
						  id: snap.key,
						  image_firebase_url: snap.val().image_firebase_url,
						  name: snap.val().name,
						  price: snap.val().price,
						  restaurant_image: snap.val().restaurant_image,
						  restaurant_lat: snap.val().restaurant_lat,
						  restaurant_long: snap.val().restaurant_long,
						  restaurant_name: snap.val().restaurant_name,
						  res_id: snap.val().res_id,
						  owner_id: snap.val().user_id,
						  cityId: snap.val().cityId,
						}); 
					

				
						
						 
					  });
					  
					  console.log(this.chats);
					  
					  this.chats = this.chats.reverse();
					  
					  
					  
					  
					  
		});
		
		
		console.log(this.chats);
		
		this.nearestHotels = [];
		
		this.nearestHotels = [];
		
		this.nearestHotels2 = [];
		
		this.nearestHotels3 = [];
		
		
		this.service.getNearestRestaurantItems().on('value', snapshot =>{
    
				console.log(snapshot.val());
				console.log(snapshot.key);
				
				var key = snapshot.key;
				
				var val = snapshot.val();
				
				
				this.nearestHotels = [];
				
				
				
				
					snapshot.forEach( snap =>{
						
						var t = snap.val().item_dis;
						
						var tArr = snap.val().item_dis.split("z");
						
						var hour = tArr[0];
						
						var minute = tArr[1];
						
		
						var selectedPrice = parseFloat(snap.val().price);

						
						
						this.nearestHotels.push({
						  
						  id: snap.key,
						  image_firebase_url: snap.val().image_firebase_url,
						  name: snap.val().name,
						  price: selectedPrice,
						  restaurant_image: snap.val().restaurant_image,
						  restaurant_lat: snap.val().restaurant_lat,
						  restaurant_long: snap.val().restaurant_long,
						  restaurant_name: snap.val().restaurant_name,
						  owner_id: snap.val().owner_id,
						  res_id: snap.val().res_id,
						  item_dis : minute,
						  cityId: snap.val().cityId,
						  
						}); 
					

						 
					  });
					console.log(this.nearestHotels);
		
				
				console.log(this.fromPrice);
				console.log(this.toPrice);
				
				
				this.GetUsers(this.nearestHotels, this.values.fromPrice , this.values.toPrice);
					
				
					
					
					
					
		
					console.log(this.nearestHotels2);
					  
		});
		
		console.log(this.nearestHotels);
		
		
		
		
	
		
		
		
		 function calcDistance(destination,destination1,res_id,snap){
			 
			  var userLists: any;
		
		
		
			firebase.auth().onAuthStateChanged(function(user) {
					  if (user) {
						var uid=user.uid;
						
						
					
						  firebase.database().ref('/users').child(uid).on('value', snapshot =>{

					var cord = snapshot.val();
					
					console.log(cord.lat);
						console.log(cord.lng);
						
						console.log(destination);
						console.log(destination1);
						
						
						var p1 = new google.maps.LatLng(destination, destination1);
						
						
						var p2 = new google.maps.LatLng(cord.lat, cord.lng);
					
						console.log("distance is "+google.maps.geometry.spherical.computeDistanceBetween(p1, p2)/1000);

						var distanceBetween= (google.maps.geometry.spherical.computeDistanceBetween(p1, p2))/1000;
						console.log(distanceBetween);
						
						if(distanceBetween < 10){
							firebase.database().ref('/cordItems').child(uid).child(res_id).update({
							  id: snap.key,
							  image_firebase_url: snap.val().image_firebase_url,
							  name: snap.val().name,
							  price: snap.val().price,
							  restaurant_image: snap.val().restaurant_image,
							  restaurant_lat: snap.val().restaurant_lat,
							  restaurant_long: snap.val().restaurant_long,
							  restaurant_name: snap.val().restaurant_name,
							  res_id: snap.val().res_id,
							  owner_id: snap.val().user_id,
						   	  item_dis : "az" + distanceBetween.toFixed(2)
							});
						}
						
						else if(distanceBetween >= 10 && distanceBetween < 100){
						
						
								firebase.database().ref('/cordItems').child(uid).child(res_id).update({
									id: snap.key,
							  image_firebase_url: snap.val().image_firebase_url,
							  name: snap.val().name,
							  price: snap.val().price,
							  restaurant_image: snap.val().restaurant_image,
							  restaurant_lat: snap.val().restaurant_lat,
							  restaurant_long: snap.val().restaurant_long,
							  restaurant_name: snap.val().restaurant_name,
							  res_id: snap.val().res_id,
							  owner_id: snap.val().user_id,
								 item_dis : "bz" + distanceBetween.toFixed(2)
							  });
							  
					  
						}
						
						else if(distanceBetween >= 100 && distanceBetween < 1000){
							
							
							firebase.database().ref('/cordItems').child(uid).child(res_id).update({
									id: snap.key,
							  image_firebase_url: snap.val().image_firebase_url,
							  name: snap.val().name,
							  price: snap.val().price,
							  restaurant_image: snap.val().restaurant_image,
							  restaurant_lat: snap.val().restaurant_lat,
							  restaurant_long: snap.val().restaurant_long,
							  restaurant_name: snap.val().restaurant_name,
							  res_id: snap.val().res_id,
							  owner_id: snap.val().user_id,
								 item_dis : "cz" + distanceBetween.toFixed(2)
							  });
							
						}
						
						else if(distanceBetween >= 1000 && distanceBetween < 10000){
							
							
							firebase.database().ref('/cordItems').child(uid).child(res_id).update({
								id: snap.key,
								image_firebase_url: snap.val().image_firebase_url,
								name: snap.val().name,
								price: snap.val().price,
								restaurant_image: snap.val().restaurant_image,
								restaurant_lat: snap.val().restaurant_lat,
								restaurant_long: snap.val().restaurant_long,
								restaurant_name: snap.val().restaurant_name,
								res_id: snap.val().res_id,
								owner_id: snap.val().user_id,
								 item_dis : "dz" + distanceBetween.toFixed(2)
							  });
							
						}
						else if(distanceBetween >= 10000 && distanceBetween < 100000){
							
							
							firebase.database().ref('/cordItems').child(uid).child(res_id).update({
								id: snap.key,
								image_firebase_url: snap.val().image_firebase_url,
								name: snap.val().name,
								price: snap.val().price,
								restaurant_image: snap.val().restaurant_image,
								restaurant_lat: snap.val().restaurant_lat,
								restaurant_long: snap.val().restaurant_long,
								restaurant_name: snap.val().restaurant_name,
								res_id: snap.val().res_id,
								owner_id: snap.val().user_id,
								 item_dis : "fz" + distanceBetween.toFixed(2)
							  });
							
						}
					  
					  
						
						return distanceBetween;
					});	
			
		
					}
				});
				
			
		  }
		
	}

  ngOnInit() {
  }
  
  searchItem(ev : any){
	   this.initializeItems();
	   
	   console.log(this.nearestHotels);
	   console.log(ev);
	   
        let val = ev.target.value;
		
		console.log(val);
		
        if (val && val.trim() != '') {
			
            //this.params.data.items = this.items.filter((data) => {
			this.nearestHotels = this.nearestHotels.filter((data) => {
				
				console.log(data);
				
                return (data.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        }
	  
  }
  
  
  initializeItems() {
	  
	  
	  this.service.getNearestRestaurantItems().on('value', snapshot =>{
    
				console.log(snapshot.val());
				console.log(snapshot.key);
				
				var key = snapshot.key;
				
				var val = snapshot.val();
				
				
				this.nearestHotels = [];
				
				
				
				
					snapshot.forEach( snap =>{
						
						var t = snap.val().item_dis;
						
						var tArr = snap.val().item_dis.split("z");
						
						var hour = tArr[0];
						
						var minute = tArr[1];
						
		
						

						
						
						this.nearestHotels.push({
						  
						  id: snap.key,
						  image_firebase_url: snap.val().image_firebase_url,
						  name: snap.val().name,
						  price: snap.val().price,
						  restaurant_image: snap.val().restaurant_image,
						  restaurant_lat: snap.val().restaurant_lat,
						  restaurant_long: snap.val().restaurant_long,
						  restaurant_name: snap.val().restaurant_name,
						  owner_id: snap.val().owner_id,
						  res_id: snap.val().res_id,
						  item_dis : minute,
						  
						  
						}); 
					

						 
					  });
					console.log(this.nearestHotels);
					  
		});
		
  }
  
  
  GetUsers(nearest , fromPrice , toPrice) {
  
  
		this.nearestHotels2 = _.filter(nearest, function(o) { 
					
					
					
					//this.fromPrice = parseFloat(this.values.fromPrice);
							
					//this.toPrice = parseFloat(this.values.toPrice);
						
						//console.log(this.fromPrice);
					//console.log(this.toPrice);
					//console.log(o.price);
					//console.log(this.fromPrice);
					//console.log(this.toPrice);
					
					//return (o.price >  this.values.fromPrice && o.price < this.values.toPrice)  
					return (o.price >=  fromPrice && o.price <= toPrice)  
					
					
		});
		
		
		this.nearestHotels3 = _.orderBy(this.nearestHotels2, ['price'],['asc']);
		
		//this.nearestHotels3 = _.sortBy(this.nearestHotels2, o => o.price);
		
		//const myOrderedArray = _.sortBy(myArray, o => o.name)
		
		console.log(this.nearestHotels3);
       // _.remove(data.result, { username: name });
        //this.allUsers = data.result;
    
  }
  
      cambiaIdioma(idioma: string) {
    //console.log(`Traduzco a: ${idioma}`);
    this.translate.use(idioma);
  }

}
