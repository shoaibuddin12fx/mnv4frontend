import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterProductsPage } from './filter-products.page';

describe('FilterProductsPage', () => {
  let component: FilterProductsPage;
  let fixture: ComponentFixture<FilterProductsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FilterProductsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterProductsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
