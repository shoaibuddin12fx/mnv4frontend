import { Component , OnInit} from '@angular/core';
import { NavController, LoadingController, ToastController, Events, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { ServiceProvider } from '../../providers/service';

import { Values } from '../../providers/values';

import { CallNumber } from '@ionic-native/call-number/ngx';

import { Router } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';

import { FormBuilder, Validators } from '@angular/forms';

import * as firebase from 'firebase';

import * as _ from 'lodash';

@Component({
  selector: 'app-filter-products',
  templateUrl: './filter-products.page.html',
  styleUrls: ['./filter-products.page.scss'],
})
export class FilterProductsPage implements OnInit {
	
	map : any;
	image: any;
	dataInfo: any;
	userList: any;
	distanceRestaurant: any;
  
	movieDetailPage = 'MovieDetailPage';
	informationDetailPage = 'InformationDetailPage';

	offset: number = 0;
  
	public layouticon:any;
	public layout:any;
	
	currentUser: any;
	myOrderList: any;
	id:any;
	
	params:any = {};

	
	searchedItem: any;
	public items: any;
	
	public chats:any = {};

	public nearestHotels:any = {};
	
	public nearestHotels2:any = {};

	public featuredHotels:any = {};
	
	
	public signupForm: any;
	
	public userProfiles: any;

  constructor(public events: Events, 
		public toastCtrl: ToastController, 
		private storage: Storage, 
		public navCtrl: NavController,
		public service: ServiceProvider,
		public loadingCtrl: LoadingController,
		private callNumber: CallNumber,
		public values: Values,
		public router: Router,
		public geo:Geolocation,
		public platform: Platform,
		public formBuilder: FormBuilder) { 
		
			this.signupForm = formBuilder.group({
							fromPrice: ['', Validators.compose([Validators.required,])],
							toPrice: ['', Validators.compose([ Validators.required])],
							
							
						});
		
		
		}

  ngOnInit() {
  }
  
  searchProducts(){
	  
	 console.log(this.signupForm.value.fromPrice);
	 
	 console.log(this.signupForm.value.toPrice);
	 
	 
	 this.values.fromPrice = this.signupForm.value.fromPrice;
	 this.values.toPrice = this.signupForm.value.toPrice;
	 
	 console.log(this.values.fromPrice);
	 console.log(this.values.toPrice);
	 
	 
	 this.router.navigateByUrl('search-products');			
	  
  }

}
