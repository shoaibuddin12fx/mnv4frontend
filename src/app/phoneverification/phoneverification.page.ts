import { NavController , AlertController , MenuController} from '@ionic/angular';
import { Component, OnInit , ViewChild} from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Storage } from '@ionic/storage';


import { ServiceProvider } from '../../providers/service';


import * as firebase from 'firebase';

import { HttpClient } from '@angular/common/http';

import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-phoneverification',
  templateUrl: './phoneverification.page.html',
  styleUrls: ['./phoneverification.page.scss'],
})
export class PhoneverificationPage implements OnInit {

  public form: FormGroup;
  public phone: any;
  private verificationId: any;
  private action: string;
  private user: any;
  public loading: any = false;
  public currentUser: any;
  public userProfiles: any;

  constructor(private param: ActivatedRoute, private nav: NavController, 
  private fb: FormBuilder, public menuCtrl: MenuController, 
  private storage: Storage, public service: ServiceProvider,private toastController: ToastController) {
      
	
	 
	  
	  this.param.queryParams.subscribe((params: any) => {
        if (params) {
          this.user = params.user ? JSON.parse(params.user) : {};
          this.action = params.action;
          this.phone = JSON.parse(params.phone);
          this.verificationId = params.verificationId;

        }
      });
	  
	  
	  
    }

  ngOnInit() {
   this.form = this.fb.group({
      code: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(6), Validators.pattern('[0-9]+')] ]
    });
  }

  verifyPhoneNumber() {
    if (this.action === 'update') {
      
        this.service.updatePhone(this.verificationId, this.form.value.code, this.phone).then( (res: string) => {
         
		  
		  
		  this.service.getRestaurantUserProfile(firebase.auth().currentUser.uid).on('value', (snapshot) =>{
	
						
						this.userProfiles = snapshot.val();
						
		
									
									let user = {
										avt: null,
										username: this.userProfiles.displayName,
										fullname: this.userProfiles.lastName,
										email: this.userProfiles.email,
										address: this.userProfiles.address,
										phone: this.userProfiles.phone,
										id: firebase.auth().currentUser.uid,

									}
									
				
									this.storage.set('user', user);

										
										
										this.menuCtrl.enable(true);
										this.nav.navigateForward(['home']);
										this.loading = false;
										
								
						
				});
		  
        }).catch(err => {
          console.log(err);
          
          this.warningToast(err);
        });
      
    } else if (this.action === 'register') {
      this.loading = true;
      this.service.registerByPhone(this.user, this.verificationId, this.form.value.code).then((res: string) => {
        this.regularInfoToast(res);
		

				
				this.service.getRestaurantUserProfile(firebase.auth().currentUser.uid).on('value', (snapshot) =>{

						
						this.userProfiles = snapshot.val();
						

									
									let user = {
										avt: null,
										username: this.userProfiles.displayName,
										fullname: this.userProfiles.lastName,
										email: this.userProfiles.email,
										address: this.userProfiles.address,
										phone: this.userProfiles.phone,
										id: firebase.auth().currentUser.uid,

									}
									
			
									this.storage.set('user', user);
									

									
										this.menuCtrl.enable(true);
										this.nav.navigateForward('home');
										this.loading = false;
								
						
				});
				
				
		
	
      }).catch(err => {
        this.loading = false;
        console.log(err);
        this.warningToast(err);
      });
    } else {

		  
        this.service.phoneAuth(this.verificationId, this.form.value.code, this.phone).then( (res: string) => {
			
			
			 this.service.getRestaurantUserProfile(firebase.auth().currentUser.uid).on('value', (snapshot) =>{
	
						this.userProfiles = snapshot.val();
						
	
									
									let user = {
										avt: null,
										username: this.userProfiles.displayName,
										fullname: this.userProfiles.lastName,
										email: this.userProfiles.email,
										address: this.userProfiles.address,
										phone: this.userProfiles.phone,
										id: firebase.auth().currentUser.uid,

									}
									
					
									this.storage.set('user', user);

									
										this.menuCtrl.enable(true);
										this.nav.navigateForward('home');
										
										
										
										
										this.loading = false;
								
						
				});
     
        }).catch(err => {
          alert(err);

		  
          this.warningToast(err);
        });
    }
  }
  
  async regularInfoToast(info: string, position: any = 'top') {
    const toast = await this.toastController.create({
      message: info,
      duration: 3000,
      position: position,
      color: 'success'
    });

    return await toast.present();
  }

  async warningToast(msg: string, css: string = 'err', position: any = 'bottom', options: boolean = true) {
    const toast = await this.toastController.create({
      color: 'danger',
      position: position,
      message: msg,
      showCloseButton: options,
      closeButtonText: 'Ok'
    });
    return await toast.present();
  }

}
